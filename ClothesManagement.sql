﻿USE master
GO
IF EXISTS(SELECT * FROM sys.databases WHERE name='ManagementClothes')
DROP DATABASE ManagementClothes
GO
CREATE DATABASE ManagementClothes
GO
USE ManagementClothes
GO

Create table [Accounts]
(
	[AccountID] Integer Identity NOT NULL,
	[UserName] Nvarchar(50) NULL,
	[PassWord] Nvarchar(50) NULL,
	[Name] Nvarchar(50) NULL,
	[PhoneNumber] Varchar(10) NULL,
	[Address] Nvarchar(50) NULL,
	[Email] Nvarchar(50) NULL,
	[Status] Tinyint NULL, -- 0 : NV , 1 AD , 2 VHH
Primary Key ([AccountID])
) 
go

Create table [Clients]
(
	[ClientID] Integer Identity NOT NULL,
	[ClientName] Nvarchar(50) NULL,
	[PhoneNumber] Varchar(10) NULL,
	[ClientAddress] Nvarchar(50) NULL,
	[Email] Nvarchar(50) NOT NULL, 
Primary Key ([ClientID])
) 
go

Create table [BillOfSales]
(
	[TransactionID] Integer Identity NOT NULL,
	[AccountID] Integer NOT NULL,
	[ClientID] Integer NOT NULL,
	[DateSale] Datetime NOT NULL,
Primary Key ([TransactionID])
) 
go

Create table [Clothes]
(
	[ClothesID] Integer Identity NOT NULL,
	[ClothesName] Nvarchar(50) NULL,
	[Price] Integer NULL,
	[Quantity] INT NOT NULL,
	[Status] Tinyint NULL,
	[SizeID] Integer NOT NULL,
Primary Key ([ClothesID])
) 
go

Create table [BillDetails]
(
	[TransactionID] Integer NOT NULL,
	[ClothesID] Integer NOT NULL,
	[Quantity] Integer NULL,
	[SalePrice] Integer NULL,
Primary Key ([TransactionID],[ClothesID])
) 
go

Create table [Suppliers]
(
	[SupplierID] Integer Identity NOT NULL,
	[SupplierName] Nvarchar(50) NULL,
	[SupplierPhone] Nvarchar(10) NULL,
	[SupplierAddress] Nvarchar(50) NULL,
	[Email] Nvarchar(50) NULL, 
	[Status] Tinyint NULL,
Primary Key ([SupplierID])
) 
go

Create table [Size]
(
	[SizeID] Integer Identity NOT NULL,
	[SizeName] Nvarchar(50) NULL,
Primary Key ([SizeID])
) 
go
go
Create table [BillImports]
(
	[TransactionID] Integer Identity NOT NULL ,
	[AccountID] Integer NOT NULL,
	[PublisherID] Integer NOT NULL REFERENCES [Suppliers]([SupplierID]),
	[CreatedDate] Datetime NULL
Primary Key ([TransactionID])
) 
GO
Create table [ImportBillDetails]
(
	[TransactionID] Integer NOT NULL,
	[ClothesID] Integer NOT NULL,
	[Quantity] Integer NULL,
	[ImportPrice] Integer NULL,
Primary Key ([TransactionID],[ClothesID])
) 

go




Alter table [BillOfSales] add  foreign key([AccountID]) references [Accounts] ([AccountID])  on update no action on delete no action 
go
Alter table [BillImports] add  foreign key([AccountID]) references [Accounts] ([AccountID])  on update no action on delete no action 
go
Alter table [BillOfSales] add  foreign key([ClientID]) references [Clients] ([ClientID])  on update no action on delete no action 
go
Alter table [BillDetails] add  foreign key([TransactionID]) references [BillOfSales] ([TransactionID])  on update no action on delete no action 
go
Alter table [BillDetails] add  foreign key([ClothesID]) references [Clothes] ([ClothesID])  on update no action on delete no action 
go
Alter table [Clothes] add  foreign key([SizeID]) references [Size] ([SizeID])  on update no action on delete no action 
go
Alter table [ImportBillDetails] add  foreign key([ClothesID]) references [Clothes] ([ClothesID])  on update no action on delete no action 
go
Alter table [ImportBillDetails] add  foreign key([TransactionID]) references [BillImports] ([TransactionID])  on update no action on delete no action 
go



Set quoted_identifier on
go


Set quoted_identifier off
go


/* Roles permissions */


/* Users permissions */



/* Roles permissions */


/* Users permissions */


INSERT INTO dbo.Accounts(UserName,PassWord,Name,PhoneNumber,Address,Email,Status)
					VALUES(   N'1', N'1',N'Vũ Tùng Lâm', '0965855165', N'Khả Phong - Kim Bảng - Hà Nam',N'lamvu221200@gmail.com', 1   )
					,(   N'2', N'2',N'Nguyễn Văn Cường', '0965855165', N'Khả Phong - Kim Bảng - Hà Nam',N'lamvu221200@gmail.com', 0  )
					,(   N'3', N'3',N'Nguyễn Mạnh Cường', '0965855165', N'Khả Phong - Kim Bảng - Hà Nam',N'lamvu221200@gmail.com', 0 )
					,(   N'4', N'4',N'Trần Quang Đạt', '0965855165', N'Khả Phong - Kim Bảng - Hà Nam',N'lamvu221200@gmail.com', 2 )
					,(   N'5', N'5',N'Lê Hồng Lâu', '0965855165', N'Khả Phong - Kim Bảng - Hà Nam',N'lamvu221200@gmail.com', 1   )
GO
INSERT INTO dbo.Size(SizeName)
				VALUES(N'XXL')
				,(N'XL')
				,(N'L')
				,(N'M')
				,(N'S')
				,(N'38')
				,(N'39')
				,(N'40')
				,(N'41')
				,(N'42')
GO
INSERT INTO dbo.Clients(ClientName,PhoneNumber,ClientAddress,Email)
VALUES(  N'Vũ Tùng Lâm', '0965855165', N'Khả Phong - Kim Bảng - Hà Nam',N'lamvu221200@gmail.com'  )
,(   N'Trần Quang Đạt', '0965855165', N'Khả Phong - Kim Bảng - Hà Nam',N'lamvu221200@gmail.com'  )
,(   N'Nguyễn Văn Cường', '0965855165', N'Khả Phong - Kim Bảng - Hà Nam',N'lamvu221200@gmail.com'  )
,(   N'Nguyễn Mạnh Cường', '0965855165', N'Khả Phong - Kim Bảng - Hà Nam',N'lamvu221200@gmail.com'  )

 GO
 INSERT INTO dbo.Clothes(ClothesName,Price,Quantity,Status,SizeID)
			 VALUES(   N'Áo khoác',900000,5,  0,   1   )
			 ,(   N'Áo khoác',800000,15,  0,   3   )
			 ,(   N'Áo khoác',850000,10,  0,   4   )
			 ,(   N'Quần bò',30000, 120,  0,   6   )
			 ,(   N'Quần kaki',50000, 1000,  0,   1   )
			 ,(   N'Quần ấu',50000,1000,   0,   2   )
			 ,(   N'Áo cộc',15000, 1000,  0,   2   )
			 ,(   N'Áo polo',25000,1000,   0,   2  )
			 ,(   N'Áo ba lỗ',10000, 1000,  0,   3  )
			 ,(   N'Áo bò',20000, 1000,  0,   3   )
			 ,(   N' Áo sơ mi',50000, 1000,  0,   3   )
			 ,(   N'Áo cộc',250000, 1000,  0,   4   )
			 ,(   N'Áo gió',50000,1000,   0,   4   )
			 ,(   N'Áo da',50000,1000,   0,   4   )
			 ,(   N'Quần đùi',50000,1000,   0,   4   )
---
GO
INSERT INTO [Suppliers] VALUES 
						(N'Atino','0123456789',N'Hà Nội','lamvu221200@gmail.com',0)
						,(N'Krik','0123456789',N'Hà Nội','lamvu221200@gmail.com',0)
						,(N'Kho','0123456789',N'Hà Nội','lamvu221200@gmail.com',0)
						,(N'Lam','0123456789',N'Hà Nội','lamvu221200@gmail.com',0)
						,(N'Xanh','0123456789',N'Hà Nội','lamvu221200@gmail.com',1)
						
GO
SELECT * FROM dbo.Clients
SELECT * FROM dbo.Clothes
SELECT * FROM dbo.Size
SELECT * FROM dbo.Accounts

select * from suppliers
