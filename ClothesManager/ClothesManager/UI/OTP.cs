﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClothesManager.UI
{
    public partial class OTP : Form
    {
        public int ID { get; set; }
        public string Email { get; set; }
        private string randomCode;

        public OTP()
        {
            InitializeComponent();
        }

        private async void OTP_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "OTP đã được gửi đến gmail:\n" + Email;
            await Task.Run(() => Send());
        }

        private void Send()
        {
            string from, pass, messageBody;
            //Gen random code
            Random rand = new Random();
            randomCode = rand.Next(999999).ToString();
            //set up mail message
            MailMessage message = new MailMessage();
            string to = Email;
            from = "keiderkun@gmail.com";
            pass = "dltvlxsdyycejrzn";
            messageBody = "OTP: " + randomCode;
            message.To.Add(to);
            message.From = new MailAddress(from);
            message.Body = messageBody;
            message.Subject = "[Shein] OTP";
            //set mail client
            SmtpClient smt = new SmtpClient("smtp.gmail.com");
            smt.EnableSsl = true;
            smt.Port = 587;
            smt.DeliveryMethod = SmtpDeliveryMethod.Network;
            smt.Credentials = new NetworkCredential(from, pass);
            try
            {
                smt.Send(message);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnXN_Click(object sender, EventArgs e)
        {
            if (txtOTP.Text == randomCode)
            {
                MessageBox.Show("Thành công!!!");
                this.Hide();
                Interface @interface = new Interface();
                @interface.ID = this.ID;
                @interface.ShowDialog();
            }
            else
            {
                MessageBox.Show("OTP không đúng!!!");
                this.Hide();
                Authen authen = new Authen();
                authen.ShowDialog();
            }
        }
    }
}