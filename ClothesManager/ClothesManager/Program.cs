﻿using ClothesManager.UI;
using System;
using System.Windows.Forms;

namespace ClothesManager
{
    internal static class Program
    {
        /// <summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Authen());
        }
    }
}